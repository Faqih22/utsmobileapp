import 'package:flutter/material.dart';
import './text_control.dart';

class TextOutput extends StatefulWidget {
  @override
  _OutputState createState() => _OutputState();
}

class _OutputState extends State<TextOutput> {
  String msg = 'Ayam';

  void _changeText() {
    setState(() {
      if (msg.startsWith('A')) {
        msg = 'Bebek';
      } else if (msg.startsWith('M')) {
        msg = 'Ayam';
      } else {
        msg = 'Murai';
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Peliaharaanku : ',
            style: new TextStyle(fontSize: 35.0),
          ),
          Control(msg),
          RaisedButton(
            child: Text(
              "Change Name",
              style: new TextStyle(color: Colors.red[200], fontSize: 20.0),
            ),
            color: Theme.of(context).primaryColor,
            onPressed: _changeText,
          ),
        ],
      ),
    );
  }
}
